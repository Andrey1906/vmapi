<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    public function up(): void
    {
        Schema::create('products', static function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('price');
            $table->integer('count');
        });


        DB::table('products')->insert([
            [
                'id' => 1,
                'title' => 'Чай',
                'price' => 13,
                'count' => 100
            ],
            [
                'id' => 2,
                'title' => 'Кофе',
                'price' => 18,
                'count' => 20
            ],
            [
                'id' => 3,
                'title' => 'Кофе с молоком',
                'price' => 21,
                'count' => 20
            ],
            [
                'id' => 4,
                'title' => 'Сок',
                'price' => 35,
                'count' => 15
            ],
        ]);
    }

    public function down(): void
    {
        Schema::dropIfExists('products');
    }
}
