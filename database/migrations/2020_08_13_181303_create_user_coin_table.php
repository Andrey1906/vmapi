<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCoinTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_coin', static function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->integer('coin_cost');
            $table->integer('count');
        });

        Schema::table('user_coin', static function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('user');
        });

        DB::table('user_coin')->insert([
            [
                'user_id' => 1,
                'coin_cost' => 1,
                'count' => 10
            ],
            [
                'user_id' => 1,
                'coin_cost' => 2,
                'count' => 30
            ],
            [
                'user_id' => 1,
                'coin_cost' => 5,
                'count' => 20
            ],
            [
                'user_id' => 1,
                'coin_cost' => 10,
                'count' => 15
            ]
        ]);
    }

    public function down(): void
    {
        Schema::dropIfExists('user');
    }
}
