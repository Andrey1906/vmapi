<?php declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up(): void
    {
        Schema::create('user', static function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('user')->insert([
            'id' => 1,
            'name' => 'user_1',
        ]);
    }

    public function down(): void
    {
        Schema::dropIfExists('user');
    }
}
