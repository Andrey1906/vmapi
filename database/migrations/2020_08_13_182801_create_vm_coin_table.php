<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVmCoinTable extends Migration
{
    public function up(): void
    {
        Schema::create('vm_coin', static function (Blueprint $table) {
            $table->integer('coin_cost');
            $table->integer('count');
        });

        DB::table('vm_coin')->insert([
            [
                'coin_cost' => 1,
                'count' => 100
            ],
            [
                'coin_cost' => 2,
                'count' => 100
            ],
            [
                'coin_cost' => 5,
                'count' => 100
            ],
            [
                'coin_cost' => 10,
                'count' => 100
            ]
        ]);
    }

    public function down(): void
    {
        Schema::dropIfExists('user');
    }
}
