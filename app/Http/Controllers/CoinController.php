<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\User;
use App\UserCoin;
use App\VmCoin;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CoinController extends Controller
{

    /**
     * @return JsonResponse
     */
    protected function get(): JsonResponse
    {
        $coins = [];
        $coins['user'] = UserCoin::where('user_id', User::FIRST_USER)->get();
        $coins['vm'] = VmCoin::all();
        return response()->json($coins);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Throwable
     */
    protected function update(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'user_coins' => 'required',
            'vm_coins' => 'required',
        ]);
        if ($validator->fails()) {
            $message = 'Не сохранено';
            return response()->json($message, 400);
        }
        $user_coins = $request->get('user_coins');
        $vm_coins = $request->get('vm_coins');

        DB::beginTransaction();

        foreach ($user_coins as $user_coin) {
            UserCoin::where('user_id', $user_coin['user_id'])
                ->where('coin_cost', $user_coin['coin_cost'])
                ->update(['count' => $user_coin['count']]);
        }

        foreach ($vm_coins as $vm_coin) {
            VmCoin::where('coin_cost', $vm_coin['coin_cost'])
                ->update(['count' => $vm_coin['count']]);
        }

        DB::commit();

        $message = 'Сохранено';

        return response()->json($message);
    }
}
