<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * @return JsonResponse
     */
    protected function get(): JsonResponse
    {
        $products = Product::all();
        return response()->json($products);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    protected function update(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'count' => 'required',
        ]);

        if ($validator->fails()) {
            $message = 'Не сохранено';
            return response()->json($message, 400);
        }

        Product::where('id', $request->get('id'))
            ->update(['count' => $request->get('count')]);

        $message = 'Сохранено';

        return response()->json($message);
    }
}
