<?php declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UserCoin
 *
 * @property int $user_id
 * @property int $coin_cost
 * @property int $count
 * @method static \Illuminate\Database\Eloquent\Builder|UserCoin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCoin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCoin query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCoin whereCoinCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCoin whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCoin whereUserId($value)
 * @mixin \Eloquent
 */
class UserCoin extends Model
{
    protected $table = 'user_coin';

    protected $fillable = ['*'];

    public $timestamps = false;
}
