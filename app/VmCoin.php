<?php declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\VmCoin
 *
 * @property int $coin_cost
 * @property int $count
 * @method static \Illuminate\Database\Eloquent\Builder|VmCoin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VmCoin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VmCoin query()
 * @method static \Illuminate\Database\Eloquent\Builder|VmCoin whereCoinCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VmCoin whereCount($value)
 * @mixin \Eloquent
 */
class VmCoin extends Model
{
    protected $table = 'vm_coin';

    protected $fillable = ['*'];

    public $timestamps = false;
}
